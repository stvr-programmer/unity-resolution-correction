﻿using UnityEngine;

[System.Serializable]
public struct AspectRatioElement
{

    public Resolutions Resolution;

    [Tooltip("Put correct X position.")]
    public float XPos;
    [Tooltip("Put correct Y position.")]
    public float YPos;
    [Tooltip("Put correct X Size.")]
    public float XSize;
    [Tooltip("Put correct Y Size.")]
    public float YSize;

    public AspectRatioElement(Resolutions resolution, float xPos, float yPos, float xSize, float ySize)
    {
        Resolution = resolution;
        XPos = xPos;
        YPos = yPos;
        XSize = xSize;
        YSize = ySize;
    }

    public string GetTargetAspect()
    {
        switch (Resolution)
        {
            case Resolutions._1920_1080:
                return SpecialElementPosition.AspectRatio(1920, 1080);

            case Resolutions._2400_1080:
                return SpecialElementPosition.AspectRatio(2400, 1080);

            case Resolutions._1080_2400:
                return SpecialElementPosition.AspectRatio(1080, 2400);

            case Resolutions._1080_1920:
                return SpecialElementPosition.AspectRatio(1080, 1920);

            case Resolutions._1024_768:
                return SpecialElementPosition.AspectRatio(1024, 768);

            case Resolutions._768_1024:
                return SpecialElementPosition.AspectRatio(768, 1024);

            case Resolutions._800_600:
                return SpecialElementPosition.AspectRatio(800, 600);

            case Resolutions._600_800:
                return SpecialElementPosition.AspectRatio(600, 800);

            default:
                return "16:9";
        }
    }
}


public enum Resolutions
{
    _2400_1080 = 0,
    _1920_1080 = 1,
    _1024_768 = 2,
    _1600_900 = 3,
    _1080_2400 = 4,
    _1080_1920 = 5,
    _768_1024 = 6,
    _900_1600 = 7,
    _800_600 = 8,
    _600_800 = 9
}